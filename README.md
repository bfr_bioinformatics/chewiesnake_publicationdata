# chewiesnake_publicationdata

Supporting data for publication of the chewieSnake software.

In this repository you can find the centralized chewieSnake analysis results for the study on 1263 Salmonella enterica samples (from Bioproject PRJEB31846) and the decentralized chewieSnake_join analysis on the same (artificially split) dataset.

The report files can be found here:

### chewieSnake

* [chewieSnake report](https://bfr_bioinformatics.gitlab.io/chewiesnake_publicationdata/chewiesnake/cgmlst_report.html)

### chewieSnake_join

* [chewieSnake_join overview report](https://bfr_bioinformatics.gitlab.io/chewiesnake_publicationdata/chewiesnake_join/report.html)

Link to three exemplary cluster reports (see Notes below)

* [chewieSnake_join cluster 0006 report](https://bfr_bioinformatics.gitlab.io/chewiesnake_publicationdata/chewiesnake_join/clustering/CT_0006/clusterreport.html)
* [chewieSnake_join cluster 0033 report](https://bfr_bioinformatics.gitlab.io/chewiesnake_publicationdata/chewiesnake_join/clustering/CT_0033/clusterreport.html)
* [chewieSnake_join cluster 0046 report](https://bfr_bioinformatics.gitlab.io/chewiesnake_publicationdata/chewiesnake_join/clustering/CT_0046/clusterreport.html)

## Other files

A number of mostly tsv files can also be found in this repository. These are the input for the reports but can also be viewed (and analysed) independently of the reports.

### chewieSnake

To be found [here](https://gitlab.com/bfr_bioinformatics/chewiesnake_publicationdata/-/tree/master/chewiesnake)

* allele_profiles.tsv
* allele_statistics.tsv
* cgmlst_report.html
* cluster_addresses.tsv
* config_chewiesnake.yaml
* distance_matrix.tsv
* grapetree_mstree.tre
* hashids.tsv
* timestamps.tsv


### chewieSnake_join

To be found [here](https://gitlab.com/bfr_bioinformatics/chewiesnake_publicationdata/-/tree/master/chewiesnake_join)

* allele_distance_matrix.tsv
* allele_profiles.tsv
* allelesheet_decentralizedanalysis.tsv
* allele_statistics.tsv
* clustering
* clustering_intercluster.tre
* cluster_summary.tsv
* config_chewiesnake_join.yaml
* distance_matrix.tsv
* grapetree_mstree.tre
* listofreports.txt
* mapping_sample2clustername.tsv
* mapping_sample2externalclustername.tsv
* mapping_sample2serovar.tsv
* orphan_samples.tsv
* report.html
* sample_cluster_information.tsv
* sample_orphan_information.tsv
* samples2serovars.tsv
* sender_sample_info.tsv
* timestamps.tsv


### Note

Due to the number of size of the individual cluster reports, not all of them are available in a rendered form. However, if you clone this repository (or download individual report files), they can be readily displayed in any browser. In general, the Chrome browser is more fluent in rendering the large reports.

* The chewieSnake reports are found in https://gitlab.com/bfr_bioinformatics/chewiesnake_publicationdata/-/tree/master/chewiesnake

* The chewieSnake_join reports are found in https://gitlab.com/bfr_bioinformatics/chewiesnake_publicationdata/-/tree/master/chewiesnake_join
